/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.example.entity.Account;
import com.example.entity.AccountFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Admin
 */
@WebService(serviceName = "paymentServices")
public class paymentServices {

    @EJB
    private AccountFacadeLocal accountFacade;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")
    
    @WebMethod(operationName = "servicePayment")
    public String servicePayment(@WebParam(name = "accountID") int accountID, @WebParam(name="total") double total){
        return accountFacade.Payment(accountID, total);
    }

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "account") Account account) {
        accountFacade.create(account);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "account") Account account) {
        accountFacade.edit(account);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "account") Account account) {
        accountFacade.remove(account);
    }

    @WebMethod(operationName = "find")
    public Account find(@WebParam(name = "id") Object id) {
        return accountFacade.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<Account> findAll() {
        return accountFacade.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<Account> findRange(@WebParam(name = "range") int[] range) {
        return accountFacade.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return accountFacade.count();
    }

    @WebMethod(operationName = "checkAccount")
    public Account checkAccount(@WebParam(name = "id") int id) {
        return accountFacade.checkAccount(id);
    }

    @WebMethod(operationName = "Payment")
    public String Payment(@WebParam(name = "id") int id, @WebParam(name = "total") double total) {
        return accountFacade.Payment(id, total);
    }
    
}
